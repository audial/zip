
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from StringIO import StringIO
from zipfile import ZipFile, ZIP_DEFLATED

@csrf_exempt
def upload_zip(request):

    if request.method == 'POST':

        data_compressed = StringIO(request.body.decode('base64'))

        with ZipFile(data_compressed, 'r') as z:

            # read the file contents from archive to memory
            data = z.read('file.txt')
            
            buff = StringIO()
            z= ZipFile(buff, "w", ZIP_DEFLATED)
            z.writestr('new_file.txt', data)
            z.close()
            return HttpResponse(buff.getvalue(), content_type='text/plain; charset=x-user-defined')

    return render_to_response('upload.html') 


