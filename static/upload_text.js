
  function compressText(rawTxt) {
      var zip = new JSZip();
      zip.file("file.txt", rawTxt, {compression: "DEFLATE", compressionOptions : {level:6}});
      content = zip.generate();
      return content;
  }

  function compressandupload(stringData, upload_url) {

    try {
    
      var compressedData = compressText(stringData);
      // making ajax request (asynchronous)
      var xhr = new XMLHttpRequest();
      xhr.open('POST', upload_url, true);
      xhr.setRequestHeader('Content-Type', 'application/zip, application/octet-stream;');

      // ajax response handler 
      xhr.onreadystatechange = function() {

        if (this.readyState != 4) return;
        
        var z = new JSZip();
        z.load(xhr.responseText);
        document.getElementById('tarea2').value = z.file('new_file.txt').asText();
      }
      xhr.send("\r\n"+compressedData);
      return true;

    } catch(err) {
      console.log(err);
      return false;
    } 
  }

